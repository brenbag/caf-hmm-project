osldir = '/mnt/raid15/bbagley/AnalysisTools/osl2.0/';

addpath(genpath('/mnt/raid15/bbagley/AnalysisTools/GLEAN-master'));
addpath(genpath('/mnt/raid15/bbagley/AnalysisTools/HMM-MAR-master'));
addpath(genpath('/mnt/raid15/bbagley/AnalysisTools/spm12'));
addpath(genpath('/mnt/raid15/bbagley/AnalysisTools/FSL509'));


data = {
        '/mnt/raid15/bbagley/MRI-MEG-for-HMM/111112/Drug/beamformed/101122_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111114/Drug/beamformed/101119_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111120/Drug/beamformed/100922_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111121/Drug/beamformed/101208_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111123/Drug/beamformed/100915_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111124/Drug/beamformed/100813_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111126/Drug/beamformed/100809_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111128/Drug/beamformed/100920_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111129/Drug/beamformed/100904_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111130/Drug/beamformed/101004_1_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111112/Drug/beamformed/101122_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111114/Drug/beamformed/101119_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111120/Drug/beamformed/100922_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111121/Drug/beamformed/101208_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111123/Drug/beamformed/100915_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111124/Drug/beamformed/100813_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111126/Drug/beamformed/100809_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111128/Drug/beamformed/100920_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111129/Drug/beamformed/100904_2_post.mat'

'/mnt/raid15/bbagley/MRI-MEG-for-HMM/111130/Drug/beamformed/101004_2_post.mat'};

    
% Directory of the GLEAN analysis:
glean_dir  = fullfile('/mnt/raid15/bbagley/CAF-GLEAN/PostCafOnly'); 

% Name for this GLEAN analysis:
glean_name = fullfile(glean_dir,'glean_caffeine_40hz.mat');
    
% Clear settings
settings = struct;

% Envelope settings:
settings.envelope.overwrite = 0;
settings.envelope.log       = 0;
settings.envelope.fsample   = 40;
settings.envelope.mask      = fullfile(osldir,'/std_masks/MNI152_T1_8mm_brain.nii.gz'); % should be the same mask as used for beamforming

% Subspace settings (using parcellation mode - see the docs for PCA)
settings.subspace.overwrite                         = 0;
settings.subspace.normalisation                     = 'none';
settings.subspace.parcellation.file                 = fullfile('/mnt/raid15/bbagley/AnalysisTools/fMRI_parcellation_ds8mm.nii'); % must be same grid as previous mask
settings.subspace.parcellation.orthogonalisation    = 'symmetric';
settings.subspace.parcellation.method               = 'spatialBasis';

% Model settings:
settings.model.overwrite   = 0;
settings.model.hmm.nstates = 8;
settings.model.hmm.nreps   = 1;

% Output settings:
settings.format   = 'nii';
settings.space    = {'voxel','parcel'};

% Set up the GLEAN settings, data paths etc:
GLEAN = glean_setup(glean_name,data,settings);

% Run the analysis:
glean_run(GLEAN)

% Post Hoc Results determination

GLEAN = glean_temporal_stats(GLEAN,settings);
GLEAN = glean_pcorr(GLEAN,settings);