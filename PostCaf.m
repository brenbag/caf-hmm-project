


% List of files for which there are structurals:
sesNum = {'1','2'} ;
sessionIDs = {'111112','111114','111120','111121','111123','111124','111126','111128','111129','111130'}';
drugIDs =    {'101122','101119','100922','101208','100915','100813','100809','100920','100904','101004'}';
placeboIDs = {'101206','101202','100908','101221','100826','100827','100822','101004','100819','100916'}';
Index = 1:(numel(sesNum)*numel(sessionIDs));
Index = reshape(Index,[numel(sesNum) numel(sessionIDs)]);
dataDir = '/mnt/raid15/raweegmeg/';
bfDir   = '/mnt/raid15/bbagley/MRI-MEG-for-HMM/';


[fifs,structurals,converted,preprocessed,africa_logs,beamformed] = deal({});
for session = 1:numel(sessionIDs)
  for Rounds = 1: numel(sesNum)  
    fifs{Index(Rounds,session)}          = [dataDir 'Caffeine/'     sessionIDs{session}      '/'    drugIDs{session} '/meg/ECHO' sesNum{Rounds} '_Post.fif'];
    structurals{Index(Rounds,session)}   = [bfDir   sessionIDs{session} '/Placebo/'                      sessionIDs{session}            '.nii'];
    converted{Index(Rounds,session)}     = [bfDir   sessionIDs{session} '/Drug/spm/'                     drugIDs{session} '_' sesNum{Rounds} '_post.mat'];
    preprocessed{Index(Rounds,session)}  = [bfDir   sessionIDs{session} '/Drug/spm/'                     drugIDs{session} '_' sesNum{Rounds} '_post.mat'];
    africa_logs{Index(Rounds,session)}   = [bfDir   sessionIDs{session} '/Drug/africa/'                  drugIDs{session} '_' sesNum{Rounds} '_post'];
    beamformed{Index(Rounds,session)}    = [bfDir   sessionIDs{session} '/Drug/beamformed/'              drugIDs{session} '_' sesNum{Rounds} '_post.mat'];
  end
end

% Preprocessing settings:
preprocessing = repmat(struct,numel(fifs),1);
[preprocessing.maxfilter]  = deal(0);
[preprocessing.downsample] = deal(250);
[preprocessing.bandpass]   = deal([0.3 58]);
[preprocessing.africa]     = deal(africa_logs{:});

% Write settings to Dataset structure:
Dataset = struct('raw',             fifs, ...
                 'converted',       converted, ...
                 'processed',       preprocessed, ...
                 'structural',      structurals, ...
                 'beamformed',      beamformed, ...
                 'scanner',         'Elekta');
        
for session = 1:numel(Dataset)  
    Dataset(session).preprocessing = preprocessing(session);
end

Dataset = Dataset(1:numel(Dataset)); % subject 9 has a crap structural
             
%% Convert, downsample and filter
for session = 1:numel(Dataset)
    
    S               = [];
    S.fif_file      = Dataset(session).raw;
    S.spm_file      = Dataset(session).converted;
    osl_convert_script(S);
        
    S               = [];
    S.D             = Dataset(session).converted;
    S.fsample_new   = Dataset(session).preprocessing.downsample;
    osl_spmfun(@spm_eeg_downsample,S)
    
    S               = [];
    S.D             = Dataset(session).converted;
    S.band          = 'bandpass';
    S.freq          = Dataset(session).preprocessing.bandpass;
    S.dir           = 'twopass';
    osl_spmfun(@spm_eeg_filter,S);
    
    S               = [];
    S.D             = Dataset(session).converted;
    S.band          = 'stop';
    S.freq          = [58 62]; % mains is at 60Hz
    S.dir           = 'twopass';
    osl_spmfun(@spm_eeg_filter,S);

end


%% OSLview
Rawfifs2={};
for session = 1:numel(Dataset)
    D = spm_eeg_load(Dataset(session).converted);
    Rawfifs2{session}= spm_eeg_load(Dataset(session).converted);
end



%% MEG AFRICA - run ICA and rejection separately

for session = 1:numel(Dataset)

    S                       = [];
    S.D                     = Dataset(session).converted;
    S.logfile               = 1;
    S.ica_file              = Dataset(session).preprocessing.africa;
    S.used_maxfilter        = Dataset(session).preprocessing.maxfilter;
    S.ident.func            = @identify_artefactual_components_manual;
    S.todo.ica              = 1;
    S.todo.ident            = 1;
    S.todo.remove           = 0;
    S.ident.do_mains        = 0;
    S.ident.do_kurt         = 1;
    S.ident.do_reject       = 0;
    S.ident.artefact_chans  = {'EOG','ECG'};
    
 
end

AfricaMegs2={};    
for session = 1:numel(Dataset)
    S.D                     = Dataset(session).converted;
    S.ica_file              = Dataset(session).preprocessing.africa;
    S.used_maxfilter        = Dataset(session).preprocessing.maxfilter;
    S.todo.ica              = 0;
    S.todo.ident            = 1;
    S.todo.remove           = 1;
    S.ident.do_reject       = 1;
    
    AfricaMegs2{session}= S;
end


%% OSLview

for session = 1:numel(Dataset)
    D = spm_eeg_load(Dataset(session).processed);
end



%% Check fiducials
FidCheck2={};
for session = 1:numel(Dataset)
    D = osl_edit_fid(Dataset(session).processed);
    FidCheck2{session} = D;
end



%% Run Coregistration
for session = 1:numel(Dataset)
    S = [];
    S.D             = Dataset(session).processed;
    S.mri           = Dataset(session).structural;
    S.useheadshape  = 1;    
    S.fid.label.nasion  = 'Nasion';
    S.fid.label.lpa     = 'LPA';
    S.fid.label.rpa     = 'RPA';
    rhino(S); 
end


%% Check Coregistration 
CheckReg2={};
for session = 1:numel(Dataset)
    D = spm_eeg_load(Dataset(session).processed);
    CheckReg2{session}= D;
end



%% Forward model:
for session = 1:numel(Dataset)
    S               = [];
    S.D             = Dataset(session).processed;
    S.forward_meg   = 'Single Shell';
    osl_forward_model(S);
end


%% Copy & filter:
for session = 1:numel(Dataset)  
    S           = [];
    S.D         = Dataset(session).processed;
    S.outfile   = Dataset(session).beamformed;
    spm_eeg_copy(S)

    S      = [];
    S.D    = Dataset(session).beamformed;
    S.band = 'bandpass';
    S.freq = [4 30];
    osl_spmfun(@spm_eeg_filter,S)
end


%% OSLview:
PostRhino2={};
for session = 1:numel(Dataset)  
    D = spm_eeg_load(Dataset(session).beamformed);
    PostRhino2{session}=D;
end 



%% Beamform:
for session = 1:numel(Dataset)         
    S                   = [];
    S.D                 = Dataset(session).beamformed;
    S.modalities        = {'MEGPLANAR'};
    S.mni_coords        = osl_mnimask2mnicoords('/mnt/raid15/bbagley/AnalysisTools/osl2.0/std_masks/MNI152_T1_8mm_brain.nii.gz');
    S.timespan          = [0 Inf];
    S.pca_order         = 150;
    S.type              = 'Scalar';
    S.inverse_method    = 'beamform';
    S.prefix            = '';
    osl_inverse_model(S)   
end

%% Variance maps
nii_files = cell(size(Dataset));
for session = 1:numel(Dataset)
    bf_file = Dataset(session).beamformed;
    D = spm_eeg_load(bf_file);
    nii_files{session} = strrep(bf_file,'.mat','.nii.gz');
    nii_quicksave(osl_source_variance(D),nii_files{session},8,2)
end
